package dataAccess

import (
	"fmt"
	"reflect"
)

func genErrorFormat(
	action string,
	str interface{},
	dai interface{},
) (err string) {
	err = fmt.Sprintf("Couldn't "+action+" %v of type %v\ndata acces info: %v",
		str,
		reflect.TypeOf(str),
		dai,
	)
	return
}

/*interface errors*/
type FindError struct {
	Structure      interface{}
	DataAccessInfo interface{}
}

func (e FindError) Error() (err string) {
	err = genErrorFormat("find", e.Structure, e.DataAccessInfo)
	return
}

type WriteError struct {
	Structure      interface{}
	DataAccessInfo interface{}
}

func (e WriteError) Error() (err string) {
	err = genErrorFormat("write", e.Structure, e.DataAccessInfo)
	return
}

type GetError struct {
	Structure     interface{}
	DataAccesInfo interface{}
}

func (e GetError) Error() (err string) {
	err = genErrorFormat("get", e.Structure, e.DataAccesInfo)
	return
}
