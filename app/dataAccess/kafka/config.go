package kafka

import (
	"log"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

var config = &kafka.ConfigMap{
	"bootstrap.servers": "localhost:9092",
}

var producer, err = kafka.NewProducer(config)

func getProducer() *kafka.Producer {
	if err != nil {
		log.Fatal(err)
	}
	return producer
}
