package algorithms

import (
	"fmt"
	s "marketinator/structures"
	"sort"
)

func ProcessBid(bid s.Order, asks []s.Order) {
	var matchedAsks []s.Order
	for _, ask := range asks {
		if ask.Volume-bid.Volume == 0 {
			matchedAsks = append(matchedAsks, ask)
		}
	}

	if len(matchedAsks) == 1 {
		/*Process transaction*/
		fmt.Printf("There is a transaction!\n%v %v\n", bid, matchedAsks[0])
	}

	match := FIFO(matchedAsks)
	fmt.Printf("There is a transaction!\n%v %v\n", bid, match)
}

func FIFO(asks s.Orders) s.Order {
	// debería ordenar del más viejo al más nuevo
	// esta parte debería estar en unit
	sort.Sort(asks)
	return asks[0]
}
