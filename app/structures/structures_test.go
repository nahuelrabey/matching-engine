package structures_test

import (
	s "marketinator/structures"
	"sort"
	"testing"
	"time"
)

func TestOrdersSort(t *testing.T) {
	first := s.Order{Key: "A1", Volume: 2, ClientId: "1", Timestamp: time.Now()}
	second := s.Order{Key: "A1", Volume: 2, ClientId: "1", Timestamp: time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC)}
	third := s.Order{Key: "A1", Volume: 2, ClientId: "1", Timestamp: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)}

	orders := s.Orders{second, first, third}

	sort.Sort(orders)
	if orders[0] == third && orders[2] == first {
		t.Errorf("Wasn't sorted from oldest to newest \n%v\n", orders)
	}
	// fmt.Println(orders)
}
