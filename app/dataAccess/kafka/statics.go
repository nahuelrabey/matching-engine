package kafka

import (
	da "marketinator/dataAccess"
	u "marketinator/utils"

	"fmt"
	"log"
	"reflect"
)

func New(dac da.DataAccessConfig) KafkaDataAccess {
	// this is bad
	// shouldn't recieve a security
	// instead, a structures object
	// that validate the kind
	_, err := dac.Validate()
	if err != nil {
		log.Fatal(u.ValidationError{
			Wanted:   fmt.Sprintf("an element of %v", AcceptedKinds),
			Received: reflect.TypeOf(dac),
		})
	}
	return KafkaDataAccess{topic: dac.Kind}
}
