package structures

import "time"

type Transaction struct {
	Volume    uint64
	Price     float64
	Bid       float64
	Ask       float64
	Buyer     string   `private:"true"`
	Sellers   []string `private:"true"`
	Timestamp time.Time
}
