package item

import (
	"encoding/json"
	"fmt"
	u "marketinator/utils"
)

type Item struct {
	key   []byte
	value []byte
}

func (i Item) Key() []byte {
	return i.key
}

func (i Item) Value() []byte {
	return i.value
}

func (i *Item) Validate() error {
	if i == nil {
		return u.ValidationError{
			Wanted:   "item not nill",
			Received: fmt.Sprintf("item: %v", i),
		}
	}
	return nil
}

func (i *Item) Unmarshal(o interface{}) (err error) {
	/*
	* Use json.Unmarshal to populate @param o
	* @param o is an empty interface
	 */
	err = json.Unmarshal(i.value, o)
	return
}

func (i *Item) Marshal(o interface{}) error {
	oBytes, err := json.Marshal(o)
	if err != nil {
		return err
	}

	i.value = oBytes
	return nil
}
