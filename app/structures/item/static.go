package item

import (
	"encoding/json"
	"fmt"
	"reflect"
)

type errorCreatingItem struct {
	rawItem      interface{}
	marshalError error
}

func (e errorCreatingItem) Error() (msg string) {
	itemType := reflect.TypeOf(e.rawItem)
	marshalError := e.marshalError.Error()
	msg = fmt.Sprintf("Error creating new item\njson.Marshal(%v) throw error:%v\n",
		itemType,
		marshalError,
	)

	// msg = fmt.Sprintf("Couldn't "+action+" %v of type %v\ndata acces info: %v",
	// 	str,
	// 	reflect.TypeOf(str),
	// 	dai,
	// )
	return
}

func New(key string, rawItem interface{}) (i *Item, err error) {
	/*
	* Returns a new Item{}
	* @params key is the key to save the item
	* @params rawItem is the item structure tah will be converted to JSON
	 */
	var itemByteData []byte
	itemByteData, err = json.Marshal(rawItem)

	if err != nil {
		return nil, errorCreatingItem{
			rawItem:      rawItem,
			marshalError: err,
		}
	}

	return &Item{key: []byte(key), value: itemByteData}, nil
}
