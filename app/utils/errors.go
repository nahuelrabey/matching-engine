package utils

import (
	"fmt"
)

type ValidationError struct {
	Wanted   interface{}
	Received interface{}
}

func (e ValidationError) Error() string {
	message := fmt.Sprintf("wanted %v, received %v", e.Wanted, e.Received)
	return message
}

type UnexpectedError struct {
	Expected interface{}
	Received interface{}
	Context  string
}

func (e UnexpectedError) Error() string {
	message := fmt.Sprintf(
		"expected: %v\nreceived: %v\ncontext: %v",
		e.Expected,
		e.Received,
		e.Context,
	)
	return message
}
