package dataAccess

import (
	"fmt"
	s "marketinator/structures"
	i "marketinator/structures/item"
	u "marketinator/utils"
	"time"
)

type IDataAccess interface {
	Find(key string) (<-chan s.Orders, error)
	Write(s i.Item) (<-chan i.Item, error)
	Get(key string, sort time.Time) (<-chan s.Security, error)
}

type DataAccessConfig struct {
	Kind string
}

var AcceptedKinds = [5]string{"bid", "ask", "stock", "option", "bound"}

func (s DataAccessConfig) Validate() (string, error) {
	/*
	* Check if it's a valid config
	 */
	matchCounter := 0
	for _, a := range AcceptedKinds {
		if a == s.Kind {
			matchCounter++
		}
	}
	// if isn't a match throw a validation error
	if matchCounter < 1 {
		return "", u.ValidationError{
			Wanted:   fmt.Sprintf("one of %v", AcceptedKinds),
			Received: s.Kind,
		}
	}
	// if matchCounter > 1 {
	// 	return "", u.UnexpectedError{
	// 		Expected: "matchCounter < 1",
	// 		Received: fmt.Sprintf("matchCounter = %v", matchCounter),
	// 		Context: "matchCounter > 1 = true",
	// 	}
	// }

	return s.Kind, nil

}
