package structures

type Security struct {
	ID        string
	Partition int
	Algorithm string
}
