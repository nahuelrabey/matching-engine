package algorithms_test

import (
	s "marketinator/structures"

	a "marketinator/algorithms"
	"math/rand"
	"sort"
	"testing"
	"time"
)

func randDate() time.Time {
	min := time.Date(1970, 0, 0, 0, 0, 0, 0, time.UTC).Unix()
	max := time.Date(2070, 0, 0, 0, 0, 0, 0, time.UTC).Unix()
	delta := max - min
	sec := min + rand.Int63n(delta)
	return time.Unix(sec, 0)
}

// esto debe estar en structures_test

func TestFIFO(t *testing.T) {
	/*
		FIFO - First In First Out
		From an orders slice select the oldest order
		return the match
	*/
	askOrders := s.Orders{
		{Key: "A1", Volume: 20000, ClientId: "1", Timestamp: randDate()},
		{Key: "A1", Volume: 20000, ClientId: "145", Timestamp: randDate()},
		{Key: "A1", Volume: 20000, ClientId: "12", Timestamp: randDate()},
	}

	// this returns one variable
	match := a.FIFO(askOrders)
	// this orders the array from older to newest
	sort.Sort(askOrders)

	// if the match isn't the oldest element
	// of the orders, then fail
	if askOrders[0] != match {
		t.Errorf("FIFO() -> %v, wants %v instead", match, askOrders[0])
	}
}
