package structures

import "time"

type Order struct {
	Key string
	// volume uint64
	Volume    int64
	ClientId  string
	Timestamp time.Time
	// kind string `accept:"bid,ask"`
}

type Orders []Order

func (o Orders) Len() int {
	return len(o)
}

func (o Orders) Swap(i, j int) {
	o[i], o[j] = o[j], o[i]
}

func (o Orders) Less(i, j int) bool {
	timeI := o[i].Timestamp
	timeJ := o[j].Timestamp
	return timeI.After(timeJ)
}
