package kafka

import (
	i "marketinator/structures/item"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

type KafkaDataAccess struct {
	topic string
}

func (da KafkaDataAccess) Write(item i.Item) (itemChan chan i.Item, err error) {

	/*
	* Writes an item to kafka
	* @params item is json parseable
	 */

	// store the item
	producer := getProducer()
	delivery_chan := make(chan kafka.Event, 1000)
	message := &kafka.Message{
		TopicPartition: kafka.TopicPartition{
			Topic: &da.topic,
			// no se puede usar un string cómo partición.
			// si se quiere usar cómo partición el id de cada clave
			// debe prepararse un diccionario de referencias
		},
		Value: item.Value(),
		Key:   item.Key(),
	}
	err = producer.Produce(message, delivery_chan)

	if err != nil {
		return nil, err
	}

	// read the message
	go func() {
		// get the event from the delivery channel
		event := <-delivery_chan
		message = event.(*kafka.Message)

		// mannage the error, close the chan if err
		if message.TopicPartition.Error != nil {
			// ¿How to mannage this error concurrently?
			close(itemChan)
		}

		// send the res through channel
		itemChan <- item
	}()

	return itemChan, nil
}
